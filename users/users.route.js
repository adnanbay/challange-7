const express = require("express");

const userRouter = express.Router();
const userModel = require('./users.model');
const userController = require('./users.controller');

userRouter.get("/user", userController.getAllUser);

// API to register a new user
userRouter.post("/registration", userController.registerUser);

// userRouter.post("/bioregistration", userController.bioRegistration);

userRouter.post("/login", userController.userLogin);

userRouter.get("/detail/:idUser", userController.getSingleUser);

userRouter.put("/detail/:idUser", userController.updateUserBio);

userRouter.post("/recordgame", userController.recordGame);

userRouter.get("/gamehistory/:idUser", userController.gameHistory);



module.exports = userRouter;