const userList = [];
const md5 = require("md5");
const { Op } = require("sequelize");

const db = require('../models');

class UserModel{
    // Dapat semua data
    getAllUsers = async () => {
        const dataUsers = await db.User.findAll({ include: [db.UserBio, db.UserGameHistory]});
        // SELECT * FROM USERS
        return dataUsers;
    };

    getSingleUser = async (idUser) => {
        return await db.User.findOne({ 
            include: [db.UserBio],
            where: { id : idUser},
        });
    };

    isUserBioExist = async (idUser) => {
        const dataAda = await db.UserBio.findOne ({
            where: {
                user_id : idUser
            },
        });

        if (dataAda){
            return true;
        } else {
            return false;
        };
    }

    createUserBio = async (idUser, fullname, address, phoneNumber, ig, twitter)=> {
        return await db.UserBio.create({
            fullname:fullname,
            address:address,
            phoneNumber:phoneNumber,
            ig:ig,
            twitter:twitter,
            user_id : idUser,
        });
    };

    updateUserBio = async (idUser, fullname, address, phoneNumber, ig, twitter)=> {
        return await db.UserBio.update(
        { fullname: fullname, address: address, phoneNumber:phoneNumber, ig:ig, twitter:twitter},
        { where: { user_id : idUser}}
        );
    };

    // Method data sudah teregistrasi atau belum
    isUserRegistered = async (dataReq) => {
        const dataAda = await db.User.findOne ({
            where: { [Op.or] : [
                { username : dataReq.username},
                { email: dataReq.email},
            ],
            },
        });
        
        // const dataAda = userList.find((data) => {
        // return (
        // data.username === dataReq.username || 
        // data.email === dataReq.email
        // );
        // });
        if (dataAda){
            return true;
        } else {
            return false;
        };
    
    };

    
    
    // Method record data
    recordNewData = (dataReq) => {
    // INSERT INTO table () values () 
    db.User.create({
    username: dataReq.username,
    email: dataReq.email,
    password: md5(dataReq.password),
     });
       
    };

    // Method input game history
    recordGame = (dataGame) => {
        db.UserGameHistory.create({
            user_id: dataGame.user_id,
            status: dataGame.status
        });
    };

    gameHistory = async (idUser) => {
        return await db.User.findOne({ 
            include: [db.UserGameHistory],
            where: { id : idUser}});
    }
    
    // Method Login
    verifyLogin = async (username, password) => {
    //     const sortedValue = userList.find((user) => {
    //     return user.username === username  && user.password === md5(password);
    // });

        const sortedValue = await db.User.findOne ({
            where: { username:username, password: md5(password)},
        });
        
        return sortedValue;
    }
    

}

module.exports = new UserModel();