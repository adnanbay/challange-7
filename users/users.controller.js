const userModel = require('./users.model');

class UserController{
    getAllUser = async (req, res) => {
        const allUsers = await userModel.getAllUsers();
        return res.json(allUsers);
    };

    getSingleUser = async (req, res) => {
      const {idUser} = req.params;
      try {
        const users = await userModel.getSingleUser(idUser);
        if (users) {
          return res.json(users);
        } else {
          res.statusCode = 400;
          return res.json({message: "User id Tidak Ditemukan: " + idUser});  
        };
        
      } catch (error) {
        res.statusCode = 400;
        return res.json({message: "User id Tidak Ditemukan: " + idUser});
      }
      
      
    };

    updateUserBio = async (req,res) => {
      // dapet id user dari params
      const {idUser} = req.params;
      // dapet fullname
      const {fullname, address, phoneNumber, ig, twitter} = req.body;
      
      // check user bio
      const existUser = await userModel.isUserBioExist(idUser);
      // jika sudah ada update
      if (existUser) {
        userModel.updateUserBio(idUser,fullname,address,phoneNumber,ig, twitter);
        // jika belum create
      } else {
        userModel.createUserBio(idUser,fullname,address,phoneNumber,ig, twitter);
      }

      // const updatedUser = await userModel.updateUserBio(idUser, fullname);
      return res.json({message: "Bio Sudah Di Update"});
    };

    registerUser = async (req, res) => {
        const dataReq = req.body;
  
    // Cek username email dan password terisi
    if (dataReq.username === undefined || dataReq.username === ""){
      res.statusCode = 400;
      return res.json({message: "Isi username"});
    }
    if (dataReq.email === undefined || dataReq.email === ""){
      res.statusCode = 400;
      return res.json({message: "Isi email"});
    }
    if (dataReq.password === undefined || dataReq.password === ""){
      res.statusCode = 400;
      return res.json({message: "Isi password"});
    }
  
     // Check data udah ada apa belum
  
    const dataAda = await userModel.isUserRegistered(dataReq);
  
    if (dataAda){
      return res.json({message: "Username atau email sudah ada"});
    }
  
     // Record user 
  
    userModel.recordNewData(dataReq);
    return res.json({ message: "Sukses menambahkan user baru!" });
    };

    // bioRegistration = async (req, res) => {
    //     const userBio = req.body;

    //     try {
    //       if (
    //         userBio.fullname == "" &&
    //         userBio.address == "" &&
    //         userBio.phoneNumber == "" &&
    //         userBio.ig == "" &&
    //         userBio.twitter == "" &&
    //         userBio.user_id == ""
    //       ) {
    //       res.statusCode = 400;
    //       return res.json({ message: "Tolong isi data dengan lengkap" });
    //       } else if(userBio.fullname == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi fullname" });
    //       } else if(userBio.address == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi address" });
    //       } else if(userBio.phoneNumber == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi phone number" });
    //       } else if(userBio.ig == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi Instagram" });
    //       } else if(userBio.twitter == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi Twitter" });
    //       } else if(userBio.user_id == "") { 
    //         res.statusCode = 400;
    //         return res.json({ message: "Tolong isi user id terdaftar", });
    //       }
    //     } catch (error) {
    //       console.log(error);
    //     }

    //     const bioExist = await userModel.isUserBioExist(userBio);
    //     console.log(bioExist);
    //     if(bioExist) {
    //       res.statusCode = 400;
    //       return res.json({message: "Data sudah ada"});
    //     }
    //     userModel.recordNewUserBio(userBio);
    //     return res.json({message: "User Bio baru berhasil ditambah"});
    // };
    
    recordGame = async (req, res) => {
      const dataGame = req.body;

      if (
        dataGame.user_id == "" &&
        dataGame.status == ""
        ) {
          return res.json({message:"Isi data dengan lengkap"});
        } else if (dataGame.user_id == "") {
          return res.json({message:"Masukan User id"});
        } else if (dataGame.status == "") {
          return res.json({message:"Masukan Status Win / Lose"});
        };

        userModel.recordGame(dataGame);
        return res.json({message: "Berhasil Record Game"});
    
    };

    gameHistory = async (req, res) => {
      const {idUser} = req.params;
      try {
        const users = await userModel.gameHistory(idUser);
        if (users) {
          return res.json(users);
        } else {
          res.statusCode = 400;
          return res.json({message: "User id Tidak Ditemukan: " + idUser});  
        };
        
      } catch (error) {
        res.statusCode = 400;
        return res.json({message: "User id Tidak Ditemukan: " + idUser});
      }
    }
    
    userLogin = async (req, res) => {
        const {username, password} = req.body;
       
        const dataLogin = await userModel.verifyLogin(username, password);
        if (dataLogin) {
          return res.json(dataLogin);
          
        }else {
          return res.json({ message: "User is not exists!" });
        }
      
    };

}

module.exports = new UserController();