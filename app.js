const express = require("express");
const md5 = require("md5");
const app = express();
const userRouter = require('./users/users.route');

app.use(express.json());
app.use(express.static('public'));

app.get((req, res) => {
  return (res.sendFile("index.html"));
}) 



app.use("/",userRouter);

app.get("/ping", function (req, res) {
  res.send("Hello World from Nodejs");
});

// API LOGIN

// app.get("/login", (req, res) => {
//   // cara 1
//   const queryData = req.query;
//   const sortedValue = userList.find((user) => {
//     let username = user.username;
//     let email = user.email;
//     let password = user.password;
//     return (username === queryData.username || email === queryData.email) && password === queryData.password;
//   });
//   if(sortedValue === undefined){
//     res.statusCode = 404;
//     return res.json({message: "Credential Tidak Ditemukan"})
//   }

//   return res.json(sortedValue);
// });

app.listen(3000, () => {
  console.log(`Example app listening on port 3000`);
});
